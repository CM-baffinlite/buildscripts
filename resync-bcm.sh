#!/bin/bash

root="$PWD"
patch_list=(
    'bootable/recovery bootable_recovery.patch'
    'frameworks/av frameworks_av.patch'
    'frameworks/base frameworks_base.patch'
    'frameworks/native frameworks_native.patch'
    'frameworks/opt/telephony frameworks_opt_telephony.patch'
    'hardware/broadcom/libbt hardware_broadcom_libbt.patch'
    'packages/apps/Camera2 packages_apps_Camera2.patch'
    'packages/apps/Settings packages_apps_Settings.patch'
    'packages/apps/Trebuchet packages_apps_Trebuchet.patch'
    'packages/services/Telephony packages_services_Telephony.patch'
    'system/core system_core.patch'
    'external/icu external_icu.patch'
)

function patch {
    local count=0

    while [ "x${patch_list[count]}" != "x" ]; do
        curr="${patch_list[count]}"
        patches=`echo "$curr" | cut -d " " -f2-`
        folder=`echo "$curr" | awk '{print $1}'`

        cd "$folder"

        if [ "$1" = "apply" ]; then
            for patch in $patches; do
                git am "$root/buildscripts/patches/$patch"
            done
        elif [ "$1" = "reset" ]; then
            git reset --hard github/cm-12.1
        fi

        cd "$root"

        count=$(( $count + 1 ))
    done
}

repo sync -f -j8
patch apply
